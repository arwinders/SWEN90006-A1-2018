package swen90006.machine;

import java.util.List;
import java.util.ArrayList;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Files;
import java.nio.file.FileSystems;

import org.junit.*;
import static org.junit.Assert.*;

public class PartitioningTests
{
/***
 * 	it should  finish early without returning a value, but it throwing
NoReturnValueException
updating the assumption that it would return noreturnvalue exception
so it would not affect the mutation analysis proces
 */
@Test(expected = NoReturnValueException.class)
 public void EC1pcLessThanZeroTest()
 {
	    List<String> list = new ArrayList<String>();
	    list.add("mov R10 5");
	    list.add("mov R12 10");
	    list.add("add R20 R10 R12");
	    list.add("jmp -20");
	    list.add("Ret R20"); 
	    Machine m = new Machine();
	    m.execute(list);
 }
/***
 * 	it should  finish early without returning a value, but it throwing
NoReturnValueException
updating the assumption that it would return noreturnvalue exception
so it would not affect the mutation analysis proces
 */
@Test(expected = NoReturnValueException.class)
public void EC2pcHigherTest()
  {
    List<String> list = new ArrayList<String>();
    list.add("mov R10 5");
    list.add("mov R12 10");
    list.add("add R20 R10 R12");
    list.add("jmp 20");//making pc higher than number of instructions
    list.add("Ret R20"); 
    Machine m = new Machine();
    m.execute(list);
  }
  
@Test(expected = InvalidInstructionException.class)
public void EC3inValidLowRegisterTest()  throws Throwable
{
	   List<String> list = new ArrayList<String>();	   
	   list.add("ret r-10");	   
	   Machine m = new Machine();
	   m.execute(list);
}
 
@Test(expected = InvalidInstructionException.class)
public void EC4inValidHighRegisterTest()  throws Throwable
 {
	   List<String> list = new ArrayList<String>();	   
	   list.add("mov r100 120");
	   list.add("ret r100");	   
	   Machine m = new Machine();
	   m.execute(list);
 }

@Test public void EC5LowerMemoryAddressTest()
{
	    List<String> list = new ArrayList<String>();
	    list.add("mov R10 5");  
	    list.add("mov R20 590");     
	    list.add("sTr R10 -50 R20");
	    list.add("ldr R12 r10 -50");
	    list.add("ret R12"); 
	    Machine m = new Machine();
	    assertEquals("Tesing for stroing reading from memory lower than range(0)",m.execute(list),0);
 }
 
@Test public void EC6HigherMemoryAddressTest() 
 {List<String> list = new ArrayList<String>();
    list.add("mov R10 5");  
    list.add("mov r12 89");
    list.add("mov R20 590");     
    list.add("sTr R10 65532 R20");
    list.add("ldr R12 r10 65532");//would return old value not new one, as new is invalid memory
    list.add("ret R12"); 
    Machine m = new Machine();
   assertEquals("Tesing for stroing reading from memory higher than range (65535)",m.execute(list),89);
 }

//Testing for value field's value lower than range (-65535)
@Test(expected = InvalidInstructionException.class)
public void EC7LowValueTest()  throws Throwable
{
	   List<String> list = new ArrayList<String>();	   
	   list.add("mov r10 -65590");
	   list.add("ret r10");	   
	   Machine m = new Machine();
	   m.execute(list);
}

@Test(expected = InvalidInstructionException.class)
public void EC8HighValueTest()  throws Throwable
{
	   List<String> list = new ArrayList<String>();	   
	   list.add("mov r10 65590");
	   list.add("ret r10");	   
	   Machine m = new Machine();
	   m.execute(list);
}

/***
 * Next two tests  for higher than and lower than register's capacity
 * which should throw arithmeticException, 
 * which are failing here, so just commenting here as it would affect the 
 * mutation analysis process
 */
/* @Test(expected = java.lang.ArithmeticException.class)//Failed test
public void EC9HigherThansRegisterCapacityTest()  throws Throwable
{
	  List<String> list = new ArrayList<String>();
	    list.add("mov R10 65530");
	    list.add("mov R12 65530");
	    list.add("mul R20 R10 R12");
	    list.add("Ret R20");   
	    Machine m = new Machine();
	    m.execute(list);	
}

@Test(expected = java.lang.ArithmeticException.class)//Failed test
public void EC10LowerThansRegisterCapacityTest()  throws Throwable
{
	  List<String> list = new ArrayList<String>();
	    list.add("mov R10 -65530");
	    list.add("mov R12 65530");
	    list.add("mul R20 R10 R12");
	    list.add("Ret R20");   
	    Machine m = new Machine();
	    m.execute(list);
	
}*/

@Test public void EC11LoadToMemoryTest()
{
	  List<String> list = new ArrayList<String>();
	  list.add("mov R10 5");  
	  list.add("mov R20 590");     
	  list.add("sTr R10 105 R20");
	  list.add("ldr R12 r10 105");
	  list.add("ret R12"); //return the value from the memory
	  Machine m = new Machine();
	  assertEquals("Testing for valid readin/writing to memory",m.execute(list),590);
}

@Test public void EC12JMPTest()
{
	  List<String> list = new ArrayList<String>();
	  list.add("mov R10 5");  
	  list.add("jmp 2");
	  list.add("mov R10 20");         
	  list.add("ret R10"); 
	  Machine m = new Machine();
	  assertEquals("Testing from valid JMP instruction",m.execute(list),5);
}

@Test public void EC13JZTest()
{
	  List<String> list = new ArrayList<String>();
	  list.add("mov R10 0");
	  list.add("mov R12 121");
	  list.add("jz R10 2");//skipped the next instruction, so R12 value stays at 121
	  list.add("mov R12 100");   
	  list.add("Ret R12");
	  Machine m = new Machine();
	  assertEquals("Testing from valid JZ instruction",m.execute(list),121);
}

@Test public void EC14BlankLineTest()
{
	  List<String> list = new ArrayList<String>();
	  list.add("mov R10 5");  
	  list.add("    ");          
	  list.add("ret R10"); 
	  Machine m = new Machine();
	  assertEquals("Testing from valid blank line",m.execute(list),5);
}

@Test public void EC15CommentLineTest()
{
	  List<String> list = new ArrayList<String>();
	  list.add("mov R10 5");  
	  list.add("mov r10 45  ;;"); 
	  list.add(";mov r10 54 hello this is comment");
	  list.add("ret R10"); 
	  Machine m = new Machine();
	  assertEquals("Testing from comment instruction",m.execute(list),45);
}

@Test(expected = InvalidInstructionException.class)
public void EC16unrecognizedInstructionTest()  throws Throwable
{
	    List<String> list = new ArrayList<String>();
	    list.add("sqrt 30");
	    list.add("ret r21");	      
	    Machine m = new Machine();
	    m.execute(list);	
}

@Test public void E17AddTest()
{
	  List<String> list = new ArrayList<String>();
	  list.add("mov R10 5");
	  list.add("mov R12 10");
	  list.add("add R20 R10 R12");
	  list.add("Ret R20");
	  Machine m = new Machine();
	  assertEquals("Testing from valid add instruction",m.execute(list),15);
}

@Test public void EC18SubtractTest()
{
	  List<String> list = new ArrayList<String>();
	  list.add("mov R10 5");
	  list.add("mov R12 110");
	  list.add("sub R20 R12 R10");
	  list.add("Ret R20");
	  Machine m = new Machine();
	  assertEquals("Testing from valid subtract instruction",m.execute(list),105);
}

@Test public void EC19MultiplyTest()
{
	  List<String> list = new ArrayList<String>();
	  list.add("mov R10 6");
	  list.add("mov R12 5");
	  list.add("mul R20 R10 R12");
	  list.add("Ret R20");
	  Machine m = new Machine(); 
	  long value = 4294836225L;  
	  assertEquals("Testing from valid multiply instruction",m.execute(list),30);
}

@Test public void EC20DivideTest()
{
	  List<String> list = new ArrayList<String>();
	  list.add("mov R10 5");
	  list.add("mov R12 121");
	  list.add("   ");
	  list.add("Div R20 R12 R10");
	  list.add("Ret R20");
	  Machine m = new Machine();
	  assertEquals("Testing from valid divide instruction",m.execute(list),24);
	}
  
/*
 loop/ just commenting it out so, it wouldnot affect mutation analysis.
 @Test(expected = NoReturnValueException.class)
public void EC21noReturnLoopTest()  throws Throwable// array out of bound exception
  {
	  List<String> list = new ArrayList<String>();
	    list.add("mov R10 90");
	    list.add("mov R12 30");
	    list.add("mul R20 R10 R12");
	    list.add("jmp 0");//loop
	    list.add("   ");   
	    Machine m = new Machine();	
	    m.execute(list);	
  }*/

@Test(expected = NoReturnValueException.class)
public void EC22noReturnInstructionTest()  throws Throwable// array out of bound exception
  {
	  List<String> list = new ArrayList<String>();
	    list.add("mov R10 90");
	    list.add("mov R12 30");
	    list.add("mul R20 R10 R12");
	    list.add("   ");   
	    Machine m = new Machine();	
	    m.execute(list);	
  }
    
}
