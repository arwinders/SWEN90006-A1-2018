package swen90006.machine;

import java.util.List;
import java.util.ArrayList;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Files;
import java.nio.file.FileSystems;

import org.junit.*;
import static org.junit.Assert.*;

public class BoundaryTests
{
  @Test public void BVA1A_pcZero()
  {
    List<String> list = new ArrayList<String>();
    Machine m = new Machine();  
    list.add("mov R10 10");
    list.add("mov R14 -1");
    list.add("add R15 R12 R14"); 
    list.add("jz R15 4");
    list.add("mov R12 1");  
    list.add("mov R10 11");  
    list.add("jmp -6");  //Setting pc to 0
    list.add("ret R10");   
    assertEquals("Valid test for adding two numbers",m.execute(list),10);
  }
  
//This test is failing in some way, It should finish early without return a value (null)
//it is return NoReturnValueException
  @Test(expected = NoReturnValueException.class)
  public void BVA1B_pcMinusOne()
  {
    List<String> list = new ArrayList<String>();
    Machine m = new Machine();
    int instructionsExecuted = m.getCount();
    instructionsExecuted = instructionsExecuted - instructionsExecuted + 1;
    String jmpInstruction  = "jmp "+instructionsExecuted; 
    list.add(jmpInstruction);//Successful, its not return value of register 20
    list.add("mov R10 0");
    list.add("mov R12 10");
    list.add("add R20 R10 R12");
    list.add("jmp -20");
    list.add("Ret R20"); 
  //  assertEquals("Should finish early without any value",m.execute(list),null);
    m.execute(list);
  }
  

 @Test public void BVA2A_RegisterNumberZeroTest() 
 {
	   List<String> list = new ArrayList<String>();	   
	   list.add("mov r0 101");
	   list.add("ret r0");	   
	   Machine m = new Machine(); 	   
	   assertEquals("Lowest number of valid register, r0",m.execute(list),101);
 }
 
 //Register number one less than lowest number
 @Test(expected = InvalidInstructionException.class)
 public void BVA2B_RegisterNumberMinusOneTest()  throws Throwable
 {
	   List<String> list = new ArrayList<String>();	   
	   list.add("mov r-1 101");
	   list.add("ret r-1");	   
	   Machine m = new Machine();	  
	   m.execute(list);	
 }
  
 @Test public void BVA3A_ZeroMemoryAddressTest()  throws Throwable
 {
	    List<String> list = new ArrayList<String>();
	    list.add("mov R10 5");  
	    list.add("mov R20 590");     
	    list.add("str R10 -5 R20");//Storing at memory address 0
	    list.add("ldr R12 r10 -5");
	    list.add("ret R12"); 
	    Machine m = new Machine();
	    assertEquals("Lowest address number for memory address test",m.execute(list),590);
 }
//reading writing from -1 memory address, no operation 
@Test public void BVA3B_MinusOneMemoryAddressTest()  throws Throwable
{
	    List<String> list = new ArrayList<String>();
	    list.add("mov R10 5");  
	    list.add("mov R12 120"); 
	    list.add("mov R20 590");     
	    list.add("str R10 -6 R20");//Storing at memory address -1
	    list.add("ldr R12 r10 -6");
	    list.add("ret R12"); //Still reading the old value of 120, not 590
	    Machine m = new Machine();
	    assertEquals("trying to store and read from memory at address one lower than lowest value",m.execute(list),120);
} 

@ Test public void BVA4A_LowestValueTest()  throws Throwable
{
	   List<String> list = new ArrayList<String>();	   
	   list.add("mov r10 -65535");
	   list.add("ret r10");	   
	   Machine m = new Machine();
	   assertEquals("testing lowest value (-65535)",m.execute(list),-65535);
}
//trying to store -65536 (one less than lowest value), throw exception
@Test(expected = InvalidInstructionException.class)
public void BVA4B_OneMinusLowestValueTest()  throws Throwable
{
	   List<String> list = new ArrayList<String>();	   
	   list.add("mov r10 -65536");
	   list.add("ret r10");	   
	   Machine m = new Machine();
	   m.execute(list);	  
}


@Test public void BVA5A_LowestRegisterCapacityValueTest()  throws Throwable
{
	   List<String> list = new ArrayList<String>();
	    list.add("mov R10 65530");
	    list.add("mov R12 -32771");//65530*-32771+(-18) = -2147483648
	    list.add("mov R14 -18");
	    list.add("mul R20 R10 R12");	   
	    list.add("add R24 R14 R20");
	    list.add("Ret R24");
	    Machine m = new Machine();
	    assertEquals("Lowest value a regiser can store test",m.execute(list),-2147483648);	
}

/*@Test(expected = java.lang.ArithmeticException.class)//Failed test,
 * Commenting it out, as failed test would affect the mutation analysis process
 */
/*public void BVA5B_MinusOneLowestRegisterCapacityValueTest()  throws Throwable
{
	   List<String> list = new ArrayList<String>();
	    list.add("mov R10 65530");
	    list.add("mov R12 -32771");//65530*-32771+(-19) = -2147483649
	    list.add("mov R14 -19");
	    list.add("mul R20 R10 R12");	   
	    list.add("add R24 R14 R20");
	    list.add("Ret R24");
	    Machine m = new Machine();
	    m.execute(list);
	 // assertEquals("trying to store one less than lowest value of register's capacity",m.execute(list),-2147483649);
}*/

@Test public void BVA6A_HighestRegisterCapacityValueTest()  throws Throwable
{
	   List<String> list = new ArrayList<String>();
	    list.add("mov R10 65530");
	    list.add("mov R12 32771");//65530*32771+17 = 2147483647
	    list.add("mov R14 17");
	    list.add("mul R20 R10 R12");	   
	    list.add("add R24 R14 R20");
	    list.add("Ret R24");	  
	    Machine m = new Machine();
	    assertEquals("trying to store highest value of register's capacity",m.execute(list),2147483647);	
}

/*@Test(expected = java.lang.ArithmeticException.class)//Failed test, 
Commenting it out, as failed test would affect the mutation analysis process  
@Test public void BVA6B_OnePlusHighestRegisterCapacityValueTest()  throws Throwable
{
	   List<String> list = new ArrayList<String>();
	    list.add("mov R10 65530");
	    list.add("mov R12 32771");//65530*32771+17 = 2147483647
	    list.add("mov R14 18");
	    list.add("mul R20 R10 R12");	   
	    list.add("add R24 R14 R20");
	    list.add("Ret R24");
	    Machine m = new Machine();
	 // assertEquals("trying to store one more than highest value of register's capacity",m.execute(list),2147483648);	
}*/

@ Test public void BVA7A_HighestValueTest()  throws Throwable
{
	   List<String> list = new ArrayList<String>();	   
	   list.add("mov r10 65535");
	   list.add("ret r10");	   
	   Machine m = new Machine();
	   assertEquals("Testing for highest value of Value field",m.execute(list),65535);
}

//Testing for one more than highest value of Value Field (65535)
@Test(expected = InvalidInstructionException.class)
public void BVA7B_OnePlusHighestValueTest()  throws Throwable
{
	   List<String> list = new ArrayList<String>();	   
	   list.add("mov r10 65536");
	   list.add("ret r10");	   
	   Machine m = new Machine();
	   m.execute(list);	  
}

@ Test public void BVA8A_HighestMemoryAddressTest()  throws Throwable
{
	  List<String> list = new ArrayList<String>();
	  list.add("mov R10 5");  
	  list.add("mov R20 590");     
	  list.add("str R10 65530 R20");//Memory Address would be 65535
	  list.add("ldr R12 r10 65530");
	  list.add("ret R12"); 
	  Machine m = new Machine();	 
	  assertEquals("Testing for highest memory address value (65535)",m.execute(list),590);
}

@Test public void BVA8B_OnePlusHighestMemoryTest()  throws Throwable
{
	 List<String> list = new ArrayList<String>();
	  list.add("mov R10 6");  
	  list.add("mov R20 590");     
	  list.add("str R10 65530 R20");//Memory Address would be 655336
	  list.add("ldr R12 r10 65530");
	  list.add("ret R12"); //Still returning 0, default value instead of 590
	  Machine m = new Machine();	
	  assertEquals("Testing for one more thanhighest memory address value (65536)",m.execute(list),0);
}

@Test public void BVA9A_HighestRegisterNumberTest()  
{
	   List<String> list = new ArrayList<String>();	   
	   list.add("mov r31 100");
	   list.add("ret r31");	   
	   Machine m = new Machine();
	   assertEquals("Test for highest valid register number, r31",m.execute(list),100);	
}

//est for on more than highest valid register number, r32
@Test(expected = InvalidInstructionException.class)
public void BVA9B_OnePlusHighestRegisterNumberTest()  throws Throwable
{
	   List<String> list = new ArrayList<String>();	   
	   list.add("mov r32 100");
	   list.add("ret r32");	   
	   Machine m = new Machine();
	   m.execute(list);	  
}

@Test public void BVA10A_HighestPCCounterTest()  throws Throwable
{
	   List<String> list = new ArrayList<String>();	  
	   list.add("jmp 6");
	   list.add("mov r3 100");
	   list.add("mov r4 100");
	   list.add("mov r5 100");
	   list.add("ret r8");
	   list.add("mov r6 20");
	   list.add("ret r4");	   
	   Machine m = new Machine();	 
	   assertEquals("Testing hightest valid value of pc (6, in this program)", m.execute(list),0);
	   
}
//Failed Test, its returning NoReturnValueException Exception
//It should terminate early without throwing any exception
//updating assumption that it would throw exception
//so the mutation test would not be affected
@Test(expected = NoReturnValueException.class)
public void BVA10B_OnePlusHighestPCCounterTest()  throws Throwable
{
	List<String> list = new ArrayList<String>();	  
	   list.add("jmp 7");
	   list.add("mov r3 100");
	   list.add("mov r4 100");
	   list.add("mov r5 100");
	   list.add("ret r8");
	   list.add("mov r6 20");
	   list.add("ret r4");	   
	   Machine m = new Machine();
	   System.out.println("program lenghtone plus highest is : "+ list.size());
	   m.execute(list);		  
}

@Test public void BVA11A_CorrectStoreReadMemoryTest()
{
	  List<String> list = new ArrayList<String>();
	  list.add("mov R10 5");  
	  list.add("mov R20 590");     
	  list.add("sTr R10 105 R20");
	  list.add("ldr R12 r10 105");
	  list.add("ret R12"); 
	  Machine m = new Machine();
	  assertEquals("Valid test correcting storing and reading value from memory",m.execute(list),590);
}

@Test public void  BVA12A_CorrectJMPTest()
{
	  List<String> list = new ArrayList<String>();
	  list.add("mov R10 5");  
	  list.add("jmp 2");
	  list.add("mov R10 20");         
	  list.add("ret R10"); 
	  Machine m = new Machine();
	  assertEquals("Testing for valid jmp instruction",m.execute(list),5);
	  assertEquals(m.getCount(),3);
}

@Test public void  BVA13A_CorrectJZTest()
{
	  List<String> list = new ArrayList<String>();
	  list.add("mov R10 0");
	  list.add("mov R12 121");
	  list.add("jz R10 2");//skipped the next instruction, so R12 value stays at 121
	  list.add("mov R12 100");   
	  list.add("Ret R12");
	  Machine m = new Machine();  
	  assertEquals("Testing for valid JZ instruction",m.execute(list),121);
	  assertEquals(m.getCount(),4);  
}

@Test public void  BVA13B_NoTCorrectJZTest()
{
	  List<String> list = new ArrayList<String>();
	  list.add("mov R10 10");
	  list.add("mov R12 121");
	  list.add("jz R10 2");//skipped the next instruction, so R12 value stays at 121
	  list.add("mov R12 100");   
	  list.add("Ret R12");
	  Machine m = new Machine(); 
	  assertEquals("Testing for incorrect JZ instruction",m.execute(list),100);
}

@Test public void  BVA14A_BlankLineTest()
{
	  List<String> list = new ArrayList<String>();
	  list.add("mov R10 5");  
	  list.add("    ");          
	  list.add("ret R10"); 
	  Machine m = new Machine();
	  assertEquals("Testing for valid blank instruction",m.execute(list),5);
}

@Test public void  BVA15A_CommentLineTest()
{
	  List<String> list = new ArrayList<String>();
	  list.add("mov R10 5");  
	  list.add("mov r10 45  ;;"); 
	  list.add(";mov r10 54 hello this is comment");
	  list.add("ret R10"); 
	  Machine m = new Machine();
	  assertEquals("Testing for valid comment line instruction",m.execute(list),45);
}

//Testing for invalid instruction, which should trow exception
@Test(expected = InvalidInstructionException.class)
public void  BVA16A_unrecognizedInstructionTest()  throws Throwable
{
	    List<String> list = new ArrayList<String>();
	    list.add("sqrt 30");
	    list.add("ret r21");	      
	    Machine m = new Machine();
	    m.execute(list);	
}

@Test public void  BVA17A_AddTest()
{
	  List<String> list = new ArrayList<String>();
	  list.add("mov R10 5");
	  list.add("mov R12 10");
	  list.add("add R20 R10 R12");
	  list.add("Ret R20");
	  Machine m = new Machine();  
	  assertEquals("Testing for valid add instruction",m.execute(list),15);
}

@Test public void  BVA18A_SubtractTest()
{
	  List<String> list = new ArrayList<String>();
	  list.add("mov R10 5");
	  list.add("mov R12 110");
	  list.add("sub R20 R12 R10");
	  list.add("Ret R20");
	  Machine m = new Machine();
	  assertEquals("VTesting for valid subract instruction",m.execute(list),105);
}

@Test public void  BVA19A_MultiplyTest()
{
	  List<String> list = new ArrayList<String>();
	  list.add("mov R10 6");
	  list.add("mov R12 5");
	  list.add("mul R20 R10 R12");
	  list.add("Ret R20");
	  Machine m = new Machine();
	  long value = 4294836225L;  
	  assertEquals("Testing for valid multiply instruction",m.execute(list),30);
}

@Test public void  BVA20A_DivideTest()
{
	  Machine m = new Machine();
	  List<String> list = new ArrayList<String>();
	  list.add("mov R10 5");
	  list.add("mov R12 121");
	  list.add("   ");
	  list.add("Div R20 R12 R10"); 
	  list.add("Ret R20");
	  assertEquals("Testing for valid divide instruction",m.execute(list),24);
	  assertEquals( m.getCount(),5);  
}
//Testing for no return value, where it should throw exception  
@Test(expected = NoReturnValueException.class)
public void BVA21A_noReturnInstructionTest()  throws Throwable
  {
	    List<String> list = new ArrayList<String>();
	    list.add("mov R10 90");
	    list.add("mov R12 30");
	    list.add("mul R20 R10 R12");
	    list.add("   "); 
	    Machine m = new Machine();	
	    m.execute(list);	
  }
}
